using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Entities;

namespace XMLParsing
{
    internal class XmlExtractor
    {
        private string _filename;

        public XmlExtractor(string filename)
        {
            _filename = filename;
        }

        public DishList ExtractDishList()
        {
            XmlReaderSettings settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            settings.ValidationType = ValidationType.DTD;

            using XmlReader xmlReader = XmlReader.Create(_filename, settings);

            DishList result = new DishList();

            foreach (var _ in SwitchBetweenSibligns(xmlReader, "Dish"))
            {
                var dish = new Dish();
                dish.Name = xmlReader.GetAttribute("Name");
                bool parseResult = true;
                parseResult &= Enum.TryParse(xmlReader.GetAttribute("Category"), out dish.Category);
                parseResult &= int.TryParse(xmlReader.GetAttribute("Servings"), out dish.Servings) && dish.Servings >= 0;
                parseResult &= int.TryParse(xmlReader.GetAttribute("CookingTime"), out int minutes) && minutes >= 0;
                dish.CookingTime = new TimeSpan(0, minutes, 0);

                foreach (var __ in SwitchBetweenSibligns(xmlReader, "Ingredient"))
                {
                    var ingredient = new Ingredient();
                    ingredient.Name = xmlReader.GetAttribute("Name");
                    ingredient.Unit = xmlReader.GetAttribute("Unit") ?? "Piece";
                    parseResult &= double.TryParse(xmlReader.GetAttribute("Count"), out ingredient.Count) && ingredient.Count >= 0;
                    dish.Ingredients.Add(ingredient);
                }
                
                if (!parseResult)
                    throw new InvalidDataException();

                result.Dishes.Add(dish);
            }

            return result;
        }

        private static IEnumerable<object> SwitchBetweenSibligns(XmlReader xmlReader, string name)
        {
            xmlReader.ReadToFollowing(name);
            do
            {
                yield return null;
            } while (xmlReader.ReadToNextSibling(name));
        }
    }
}