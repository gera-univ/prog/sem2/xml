﻿using System;
using System.IO;
using System.Xml;
using Entities;

namespace XMLParsing
{
    class Program
    {
        static void Main(string[] args)
        {
            var extractor = new XmlExtractor("Input.xml");

            try
            {
                DishList dishList = extractor.ExtractDishList();
                Console.WriteLine(dishList);
            }
            catch (System.Xml.Schema.XmlSchemaException e)
            {
                Console.WriteLine($"XML validation error on line {e.LineNumber}");
            }
            catch (XmlException e)
            {
                Console.WriteLine($"XML parsing error on line {e.LineNumber}");
            }
            catch (InvalidDataException)
            {
                Console.WriteLine($"Incorrect numeric data");
            }
        }
    }
}