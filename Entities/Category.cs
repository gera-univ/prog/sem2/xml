using System;

namespace Entities
{
    public enum Category
    {
        DayToDay,
        Premium,
        Exclusive
    }
}