using System;
using System.Collections.Generic;
using System.Security.Cryptography;

namespace Entities
{
    public class Dish
    {
        public string Name;
        public Category Category;
        public TimeSpan CookingTime;
        public int Servings;
        public List<Ingredient> Ingredients = new List<Ingredient>();

        public Dish()
        {
        }

        public override string ToString()
        {
            string result =
                $"Dish\nName: {Name}\nCategory: {Category}\nCooking time: {CookingTime}\nMinimum servings: {Servings}\nIngredients:\n";
            
            foreach (var ingredient in Ingredients)
            {
                result += $"{ingredient}\n";
            }

            return result;
        }
    }
}