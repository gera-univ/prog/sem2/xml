﻿using System;
using System.Collections.Generic;

namespace Entities
{
    public class DishList
    {
        public List<Dish> Dishes = new List<Dish>();

        public override string ToString()
        {
            var result = "";
            foreach (var dish in Dishes)
            {
                result += $"{dish}\n";
            }

            return result;
        }
    }
}