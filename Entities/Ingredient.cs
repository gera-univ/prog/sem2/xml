namespace Entities
{
    public struct Ingredient
    {
        public string Name;
        public string Unit;
        public double Count;

        public Ingredient(string name, double count) : this()
        {
            Name = name;
            Count = count;
        }

        public Ingredient(string name, string unit, double count)
        {
            Name = name;
            Unit = unit;
            Count = count;
        }

        public override string ToString()
        {
            return $"Ingredient\nName:\t\t\t\t{Name}\nUnit:\t\t\t\t{Unit}\nCount:\t\t\t\t{Count}";
        }
    }
}